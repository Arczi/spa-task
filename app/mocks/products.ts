import { ProductItem } from '../mods/product/model/ProductItem';

export default [
    new ProductItem(1, 'Produkt 1', 'Dzien dobry, jestem produktem 1', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
    new ProductItem(2, 'Produkt 2', 'Dzien dobry, jestem produktem 2', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
    new ProductItem(2, 'Produkt 2', 'Dzien dobry, jestem produktem 2', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
    new ProductItem(2, 'Produkt 2', 'Dzien dobry, jestem produktem 2', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
    new ProductItem(2, 'Produkt 2', 'Dzien dobry, jestem produktem 2', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
    new ProductItem(2, 'Produkt 2', 'Dzien dobry, jestem produktem 2', 'https://placeimg.com/300/300/any', 'https://placeimg.com/1366/768/any', 11, 101, '2019-06-17 16:11:44', '2019-06-17 16:11:44'),
]