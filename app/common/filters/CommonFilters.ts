
export class CommonFilters {

    static truncateText(item: any, maxLength: number, active: boolean = true): string {
        if (!active) {
            return item;
        }

        if (!item) {
            return '';
        }

        return item.length > maxLength ? `${item.slice(0, maxLength)}...` : item;
    }
}
