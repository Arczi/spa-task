import { CommonFilters } from './../../../common/filters/CommonFilters';
import { ProductService } from './../ProductService';
import { Vue, Component } from 'vue-property-decorator';
import withRender from './productsList.html';
import './productsList.scss'

@withRender
@Component({
    filters: {
        CommonFilters,
    }
})
export class ProductsList extends Vue {
    productsList = [];

    async mounted() {
        this.productsList = await ProductService.getProducts();
    }

    get truncateText() {
        return CommonFilters.truncateText;
    }
}