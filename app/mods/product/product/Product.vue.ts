import { ProductService } from './../ProductService';
import { ProductItem } from './../model/ProductItem';
import Vue from "vue";
import { Component, Prop } from "vue-property-decorator";
import WithRender from './product.html';
import './product.scss';
import { Mutation } from "vuex-class/lib/bindings";
import { CartItem } from 'store/cart/model/CartItem';

@WithRender
@Component({

})
export class Product extends Vue {
    @Prop({ required: true }) id: string;
    @Mutation('CartStore/addProductToCart') addProductToCart;

    product: ProductItem = null;

    async created() {
        this.product = await ProductService.getProduct(parseInt(this.id));
    }
    
    get CartItem() {
        return CartItem;
    }



}
