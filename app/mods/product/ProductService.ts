import productsList from '../../mocks/products';
import { ProductItem } from './model/ProductItem';
import { Ajax } from 'utils/ajax';
import { ObjectMapper } from "json-object-mapper";

export class ProductService {
    static ENPOINT_URL = 'http://localhost:4000/graphql';

    static async getProducts(): Promise<ProductItem[]> {
        const query = `{allProducts{id,description,name,pricePln,image}}`;
        return Ajax.post(ProductService.ENPOINT_URL, { query }).then(products => ObjectMapper.deserializeArray(ProductItem, products.data.allProducts));
    }

    static async getProduct(id: number): Promise<ProductItem> {
        const query = `{getProduct(id:${id}){id,description,name,pricePln,imageBig}}`;
        return Ajax.post(ProductService.ENPOINT_URL, { query }).then(product => ObjectMapper.deserialize(ProductItem, product.data.getProduct));
    }

}