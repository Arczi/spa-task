export class ProductItem {
    id: number = Infinity;
    name: string = '';
    description: string = '';
    image: string = '';
    imageBig: string = '';
    quantity: number = Infinity;
    pricePln: Number = Infinity;
    createdAt: Date = new Date();
    updatedAt: Date = new Date();

    constructor(id: Number, name: String, description: String, image: String, imageBig: String, quantity: Number, pricePln: Number, createdAt: Date, updatedAt: Date) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.imageBig = imageBig;
        this.quantity = quantity;
        this.pricePln = pricePln;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}