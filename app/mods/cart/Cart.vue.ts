import Vue from "vue";
import Component from "vue-class-component";
import { Getter, Mutation } from "vuex-class/lib/bindings";
import { CartItem } from "store/cart/model/CartItem";
import withRender from './cart.html';
import './cart.scss';

@withRender
@Component
export class Cart extends Vue {
    @Getter('CartStore/cart') cartItems: CartItem[];
    @Getter('CartStore/expanded') expanded: boolean;
    @Mutation('CartStore/toggleCart') toggleCart: Function;

}