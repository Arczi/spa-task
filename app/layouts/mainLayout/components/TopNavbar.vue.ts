import { Component } from "vue-property-decorator";
import Vue from "vue";
import withRender from './topNavbar.html';
import './topNavbar.scss';
import { Getter, Mutation } from "vuex-class/lib/bindings";
import { CartItem } from "store/cart/model/CartItem";

@withRender
@Component
export class TopNavbar extends Vue {
    @Getter('CartStore/cart') cartItems: CartItem[];
    @Getter('CartStore/expanded') expanded: boolean;
    @Mutation('CartStore/toggleCart') toggleCart: Function;

    get cartAmount(): number {
        let amount = 0;
        this.cartItems.forEach((element: CartItem) => {
            amount += element.amount;
        });

        return amount;
    }
}