import { TopNavbar } from './components/TopNavbar.vue';
import { BaseLayout } from 'layouts/BaseLayout.vue';
import WithRender from './mainLayout.html';
import { Component } from 'vue-property-decorator';
import { Getter, Mutation } from 'vuex-class/lib/bindings';
import { Cart } from 'mods/cart/Cart.vue';


@WithRender
@Component({
    components: {
        TopNavbar,
        Cart
    }
})
export class MainLayout extends BaseLayout {
    get title(): string {
        return 'Welcome to vue-typescript project!';
    }

    @Getter('CartStore/mask') mask: boolean;
    @Mutation('CartStore/toggleMask') toggleMask: Function;

}
