import { Vue, Component } from 'vue-property-decorator';
import './baseLayout.scss';

export abstract class BaseLayout extends Vue {
    abstract get title(): string;
}
