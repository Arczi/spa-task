import store from 'store';
import { MainLayout } from 'layouts/mainLayout/MainLayout.vue';
import Vue from 'vue';
import router from 'router/routes';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
//in browser script tag:

Vue.use(BootstrapVue);

new Vue({
    el: '#app',
    store,
    template: '<div id="app"><main-layout></main-layout></div>',
    components: {
        MainLayout,
    },
    mounted() {
        // You'll need this for renderAfterDocumentEvent.
        document.dispatchEvent(new Event('render-event'))

    },
    beforeCreate() {
        this.$store.commit('CartStore/initialiseStore');
    },
    router,
});
