
export class CartItem {
    public productId: number;
    public productName: string;
    public productPrice: number;
    public amount: number;

    constructor(productId: number, productName: string, productPrice: number, amount: number = 1) {
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.amount = amount;
    }
}
