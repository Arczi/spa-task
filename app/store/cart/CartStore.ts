import { CartItem } from './model/CartItem';

export const CartStore = () => {
    return {
        namespaced: true,
        state: {
            items: [],
            expanded: false,
            mask: false,
        },
        getters: {
            cart(state): CartItem[] {
                return state.items;
            },
            expanded(state): boolean {
                return state.expanded;
            },
            mask(state): boolean {
                return state.mask;
            },

        },
        mutations: {
            addProductToCart(state, cartItem: CartItem): void {
                const index = state.items.findIndex((item: CartItem) => item.productId === cartItem.productId));
                if (index !== -1) {
                    state.items[index].amount++;
                } else {
                    state.items.push(cartItem);
                }

                window.localStorage.setItem('cart', JSON.stringify(state.items));
            },
            toggleCart(state) {
                state.expanded = !state.expanded;
                state.mask = state.expanded;
            },
            toggleMask(state) {
                state.mask = !state.mask;
                state.expanded = state.mask;
            },
            initialiseStore(state) {
                if (localStorage.getItem('cart')) {
                    const items = JSON.parse(localStorage.getItem('cart'));
                    items.forEach(element => {
                        state.items.push(element);
                    });
                }
            }
        },
        actions: {

        }
    }
};
