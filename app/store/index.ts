import Vuex from 'vuex';
import Vue from 'vue';
import { CartStore } from './cart/CartStore';


Vue.use(Vuex);
const store = {
    modules: {
        CartStore: CartStore(),
    },
}

export default new Vuex.Store(store);
