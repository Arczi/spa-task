import { EventBus } from 'common/eventBus/EventBus.vue';
import axios from 'axios';
import swal from 'sweetalert2';

export enum LoaderMask {
    NO_LOADER = 0,
    CURSOR_LOADER = 1,
    MASK_LOADER = 2,
}

export enum EnumErrorHandler {
    POPUP_ERRORS = 0,
    CUSTOM_HANDLER = 1,
}

export class Ajax {

    public static readonly ERROR_HANDLED = 'The error has been handled. Please do not respond to it.';

    static send(method: String, url: String, data: Object = null, loaderMaskMode: LoaderMask = LoaderMask.CURSOR_LOADER,
        errorHandler: EnumErrorHandler | Form = EnumErrorHandler.POPUP_ERRORS, args = {}, returnFullResponse: boolean = false, headers = null) {
        let finalArgs: object = Object.assign({
            url: url,
            method,
            headers: headers ? headers : { 'X-Requested-With': 'XMLHttpRequest' },
        }, args);
        if (data) {
            finalArgs = {
                ...finalArgs, ...{ data }
            };
        }

        EventBus.$emit('requestStart', url);
        const promise = axios(finalArgs).catch((error) => {
            EventBus.$emit('requestStop', url);
            if (error.response) {
                swal.fire('Error', `Internal server error <br> ${error.response.data}`, 'error');
                this.handleError(error.response, errorHandler);
            } else if (error.request) {
                swal.fire('Error', 'Connection error error', 'error');
            } else {
                swal.fire('Error', 'Connection error error', 'error');
            }

            error.handled = true;

            return error;
        }).then((response) => {
            EventBus.$emit('requestStop', url);
            if (response.handled) {
                throw response;
            }

            return returnFullResponse ? response : response.data;
        });

        return promise;
    }


    static get(url, loaderMaskMode?, errorHandler?, args?) {
        return this.send('get', url, {}, loaderMaskMode, errorHandler, args);
    }

    static post(url, data, loaderMaskMode?, errorHandler?, args?) {
        return this.send('post', url, data, loaderMaskMode, errorHandler, args);
    }

    static put(url, data, loaderMaskMode?, errorHandler?, args?) {
        return this.send('put', url, data, loaderMaskMode, errorHandler, args);
    }

    static delete(url, data, loaderMaskMode?, formComponent?, args?) {
        return this.send('delete', url, data, loaderMaskMode, formComponent, args);
    }

    static handleError(errorBody, errorHandler) {
        if (errorHandler === EnumErrorHandler.CUSTOM_HANDLER) {
            return;
        } else if (errorBody.status === 401) {
        } else if (errorBody.status === 404) {

        } else if (errorBody && errorBody.data && errorBody.data.data) {
            this.showFormErrors(errorBody.data.data, errorHandler);
        }
    }

    static showErrorsAlert(component, errors, titleParam?) {
        const title = titleParam || 'error';
        let message = '';

        errors.forEach((error) => {
            message += error.message + '</br>';
        });

        swal({ title, html: message, type: 'error' });
    }
};


