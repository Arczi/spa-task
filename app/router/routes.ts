import VueRouter from 'vue-router';
import Vue from 'vue';
import { ProductsList } from 'mods/product/list/ProductsList.vue';
import { Product } from 'mods/product/product/Product.vue';

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/product/:id', component: Product, props: true,
        },
        {
            path: '/', component: ProductsList,
        },
    ],
},
);