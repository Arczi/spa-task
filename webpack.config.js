var path = require("path");
var webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
const PrerenderSPAPlugin = require("prerender-spa-plugin");
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;
var MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PATHS = require("./conf/paths");

module.exports = {
  mode: process.env.NODE_ENV,
  entry: "./app/index.ts",
  output: {
    path: path.resolve(__dirname, "./dist"),
    publicPath: "/",
    filename: "build.js"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: {
          loader: "vue-loader?sourceMap",
          options: {
            esModule: true,
            js: [
              {
                loader: "cache-loader"
              },
              {
                loader: "thread-loader",
                options: {
                  // there should be 1 cpu for the fork-ts-checker-webpack-plugin
                  workers: require("os").cpus().length - 1
                }
              },
              {
                loader: "babel-loader",
                options: {
                  presets: ["es2015"]
                }
              }
            ]
          }
        }
      },

      {
        test: /\.tsx?$/,
        exclude: /node_modules|bower_components/,
        use: [
          {
            loader: "cache-loader"
          },
          {
            loader: "thread-loader",
            options: {
              // there should be 1 cpu for the fork-ts-checker-webpack-plugin
              workers: require("os").cpus().length - 1
            }
          },
          {
            loader: "ts-loader",
            options: {
              happyPackMode: true, // IMPORTANT! use happyPackMode mode to speed-up compilation and reduce errors reported to webpack
              appendTsSuffixTo: [/\.vue$/],
              transpileOnly: true
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader?sourceMap", "postcss-loader?sourceMap"]
      },

      {
        test: /\.html$/,
        
        include: [path.resolve(__dirname, "app")],
        exclude: [path.resolve(__dirname, "dist")],
        use: {
          loader: "vue-template-loader",
          options: {
            hmr: false
          },
        }
      },
      {
        test: /\.(woff(2)?|ttf|jpg|eot|svg)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "dist/"
            }
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      config: PATHS.config,
      layouts: PATHS.layouts,
      sass: PATHS.sass,
      assets: PATHS.assets,
      vue$: "vue/dist/vue.esm.js",
      router: PATHS.router,
      app: PATHS.app,
      mods: PATHS.mods,
      common: PATHS.common,
      components: PATHS.components,
      utils: PATHS.utils,
      store: PATHS.store
    },
    extensions: [".ts", ".js", ".vue"]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: false
  }
};
if (process.env.NODE_ENV === "production") {
  //   module.exports.devtool = "#source-map";
  module.exports.module.rules = module.exports.module.rules.concat([
    {
      test: /\.scss/,
      use: [MiniCssExtractPlugin.loader, "css-loader?sourceMap", "postcss-loader?sourceMap", "sass-loader?sourceMap"]
    }
  ]);
  module.exports.plugins = (module.exports.plugins || []).concat([
    new MiniCssExtractPlugin({
      filename: "styles.css"
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"production"'
      }
    }),
    new HtmlWebpackPlugin({
      template: "index.html",
      filename: path.resolve(__dirname, "dist/index.html"),
      chunksSortMode: "none"
    }),
    new PrerenderSPAPlugin({
      staticDir: path.join(__dirname, "dist"),
      routes: ["/"],

      renderer: new Renderer({
        headless: true,
        renderAfterDocumentEvent: 'render-event',
      })
    })
  ]);
} else {
  // NODE_ENV === 'development'
  module.exports.devtool = "#source-map";
  module.exports.module.rules = module.exports.module.rules.concat([
    {
      test: /\.scss$/,
      use: ["style-loader", "css-loader?sourceMap", "postcss-loader?sourceMap", "sass-loader?sourceMap"]
    }
  ]);
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: '"development"'
      }
    }),
    new HtmlWebpackPlugin({
      title: "DEVELOPMENT prerender-spa-plugin",
      template: "index.html",
      filename: "index.html"
    })
  ]);
}
