const path = require('path');
const currentDir = path.join(__dirname, '..')
module.exports = {
    app: path.join(currentDir, 'app'),
    lib: path.join(currentDir, 'lib'),
    bower: path.join(currentDir, 'bower_components'),
    mods: path.join(currentDir, 'app', 'mods'),
    utils: path.join(currentDir, 'app', 'utils'),
    components: path.join(currentDir, 'app', 'components'),
    dicts: path.join(currentDir, 'app', 'dicts'),
    layouts: path.join(currentDir, 'app', 'layouts'),
    sass: path.join(currentDir, 'app', 'sass'),
    config: path.join(currentDir, 'app', 'config'),
    assets: path.join(currentDir, 'app', 'assets'),
    dictsDst: path.join(currentDir, 'dist', 'static', 'dicts'),
    store: path.join(currentDir, 'app', 'store'),
    router: path.join(currentDir, 'app', 'router'),
    filters: path.join(currentDir, 'app', 'filters'),
    common: path.join(currentDir, 'app', 'common'),
    enums: path.join(currentDir, 'app', 'enums'),
    backend: path.join(currentDir, '..', 'puig-book', 'ui.apps/src/main/content/jcr_root/apps/puig-book/components/'),
};
